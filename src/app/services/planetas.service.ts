import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ApiService } from './api.service';
import { ListagemPaginadaResponse } from '@app/models/responses/lisagem.paginada.response';
import { PlanetaDetalharResponse } from '@app/models/responses/planeta.detalhar.response';
import { map } from 'rxjs/operators';
import { ListagemMapper } from '@app/models/mapeamentos/listagens';
import { PlanetaMapper } from '@app/models/mapeamentos/planetas';

@Injectable({ providedIn: 'root' })
export class PlanetasService {
  constructor(private http: HttpClient, private apiService: ApiService) { }

  url = this.apiService.apiUrl;

  listar(filtros: any) {
    var params: HttpParams = new HttpParams().set('page', filtros.pagina.toString());

    return this.http.get<ListagemPaginadaResponse<PlanetaDetalharResponse>>(this.url + 'planets', { params })
      .pipe(map(
        (res: ListagemPaginadaResponse<PlanetaDetalharResponse>) => {
          var ent = ListagemMapper.paraEntidade(res)
          res.results.forEach(r => ent.registros.push(PlanetaMapper.paraEntidade(r)))
          return ent;
        }));
  }

  detalhar(_planeta: string) {
    return this.http.get<PlanetaDetalharResponse>(this.url + 'planets/' + _planeta);
  }
}
