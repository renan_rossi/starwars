import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ApiService } from './api.service';
import { ListagemPaginadaResponse } from '@app/models/responses/lisagem.paginada.response';
import { map } from 'rxjs/operators';
import { ListagemMapper } from '@app/models/mapeamentos/listagens';
import { FilmeDetalharResponse } from '@app/models/responses/filme.detalhar.response';
import { FilmeMapper } from '@app/models/mapeamentos/filmes';
import { ListagemPaginada } from '@app/models/entidades/listagem.paginada';
import { Filme } from '@app/models/entidades/filme';

@Injectable({ providedIn: 'root' })
export class FilmesService {
  constructor(private http: HttpClient, private apiService: ApiService) { }

  url = this.apiService.apiUrl;

  listar(filtros: any) {
    var params: HttpParams = new HttpParams().set('page', filtros.pagina.toString());

    return this.http.get<ListagemPaginadaResponse<FilmeDetalharResponse>>(this.url + 'films', { params })
      .pipe(map(
        (res: ListagemPaginadaResponse<FilmeDetalharResponse>) => {            
          var ent = ListagemMapper.paraEntidade(res)
          res.results.forEach(r => ent.registros.push(FilmeMapper.paraEntidade(r)))
          return ent;
        }))
        .pipe(map((res: ListagemPaginada<Filme>) => {
            res.registros.sort((f1, f2) => { 
                let ordem = 0;
                if(+f1.id_episodio > +f2.id_episodio) ordem = 1
                if(+f1.id_episodio < +f2.id_episodio) ordem = -1

                return ordem;
            })
            return res
        }));
  }
}
