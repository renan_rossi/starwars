import { Component, OnInit } from '@angular/core';
import { PlanetasService } from '@app/services/planetas.service';
import { ToastrService } from 'ngx-toastr';
import { Planeta } from '@app/models/entidades/planeta';
import { FilmesService } from '@app/services/filmes.service';
import { Filme } from '@app/models/entidades/filme';
import { slideRightAndFadeIn, fadeInFadeOut } from '@app/shared';

@Component({
    selector: 'app-planetas-aleatorios',
    templateUrl: './planetas.aleatorios.component.html',
    styleUrls: ['./planetas.aleatorios.component.scss'],
    animations: [slideRightAndFadeIn, fadeInFadeOut]
})
export class PlanetasAleatoriosComponent implements OnInit {
    constructor(
        private planetasService: PlanetasService,
        private filmesService: FilmesService,
        private toastr: ToastrService
    ) { }

    ngOnInit() {
        this.randomizarPlaneta();
    }

    dados = {
        filmes: Array<Filme>(),
        planeta: new Planeta(),
        planetas: Array<Planeta>(),
        totalPlanetas: 0,
        qtdPlanetasPagina: 0,
        carregando: {
            planetas: true,
            filmes: true
        }
    };

    vm = {
        background: './assets/img/stars-bg.png',
    };

    private async randomizarPlaneta() {
        let pagina: number;
        if (this.dados.totalPlanetas == 0) {
            pagina = 1
        } else {
            pagina = Math.floor(Math.random() * (this.dados.totalPlanetas / this.dados.qtdPlanetasPagina) + 1)
        }

        await this.listarPlanetas(pagina)
        this.dados.planeta = this.dados.planetas[(Math.floor(Math.random() * this.dados.planetas.length))];
        await this.listarFilmes(1)
        this.dados.planeta.referenciarFilmes(this.dados.filmes);
    }

    private async listarPlanetas(pagina: number) {
        this.dados.carregando.planetas = true;
        var listagem = await this.planetasService.listar({ pagina }).toPromise()

        this.dados.planetas = listagem.registros;
        this.dados.totalPlanetas = listagem.total;
        if (this.dados.qtdPlanetasPagina == 0) {
            this.dados.qtdPlanetasPagina = listagem.registros.length;
        };
        this.dados.carregando.planetas = false;

        return;
    }

    private async listarFilmes(pagina: number) {
        this.dados.carregando.filmes = true;
        var listagem = await this.filmesService.listar({ pagina }).toPromise();

        this.dados.filmes = listagem.registros;
        this.dados.carregando.filmes = false;

        return;
    }

    private abreviarQuantidade(valor: string, fixed : number): string {
        if (isNaN(+valor)) return valor;
        else {
            var num = +valor;
            if (num === null) { return null; } // terminate early
            if (num === 0) { return '0'; } // terminate early
            fixed = (!fixed || fixed < 0) ? 0 : fixed; // number of decimal places to show
            var b = (num).toPrecision(2).split("e"), // get power
                k = b.length === 1 ? 0 : Math.floor(Math.min(+b[1].slice(1), 14) / 3), // floor at decimals, ceiling at trillions
                c = k < 1 ? num.toFixed(0 + fixed) : (num / Math.pow(10, k * 3) ).toFixed(1 + fixed), // divide by power
                d = +c < 0 ? c : Math.abs(+c), // enforce -0 is 0
                e = d + ['', 'K', ' Million', ' Billion', ' Trillion', 'Quadrillion'][k]; // append power
            return e;
        }
    }
}
