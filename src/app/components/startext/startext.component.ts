import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-startext',
  templateUrl: './startext.component.html',
  styleUrls: ['./startext.component.scss']
})
export class StartextComponent implements OnInit {
  constructor(){};
  @Input() episodio: string;
  @Input() titulo: string;
  @Input() texto: string;

  ngOnInit() {}
}
