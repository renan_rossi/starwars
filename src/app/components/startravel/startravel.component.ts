import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import  {Vec3} from 'vec3';

@Component({
    selector: 'app-startravel',
    templateUrl: './startravel.component.html'
})
export class StarTravelComponent {

    canvas:HTMLCanvasElement;
    ctx:CanvasRenderingContext2D | null;

    @ViewChild('starfield', { static: false }) starfield: ElementRef<HTMLCanvasElement>;
    constructor() { 
        this.canvas = <HTMLCanvasElement>document.getElementById('starfield');
    };

    ngAfterViewInit() {
        let canvas = this.starfield.nativeElement;
        this.ctx = canvas.getContext("2d");
      }


    animar() {
        var flr = Math.floor;
        
        this.canvas.width = this.canvas.offsetWidth;
        this.canvas.height = this.canvas.offsetHeight;

        var halfw = this.canvas.width / 2,
            halfh = this.canvas.height / 2,
            step = 2,
            warpZ = 12,
            speed = 0.075;
        var stampedDate = new Date();


        this.ctx.fillStyle = 'black';
        this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);

        function rnd(num1 : number, num2 : number) {
            return flr(Math.random() * num2 * 2) + num1;
        }

        function getColor() {
            return 'hsla(200,100%, ' + rnd(50, 100) + '%, 1)';
        }

        class Star{
            
            v = new Vec3(rnd(0 - halfw, halfw), rnd(0 - halfh, halfh), rnd(1, warpZ));


            x = this.v[0];
            y = this.v[1];
            z = this.v[2];
            color = getColor();


            reset = function () {
                this.v = new Vec3(rnd(0 - halfw, halfw), rnd(0 - halfh, halfh), rnd(1, warpZ));

                this.x = this.v[0];
                this.y = this.v[1];
                this.color = getColor();
                this.vel = this.calcVel();
            }

            calcVel = function () {

                return new Vec3(0, 0, 0 - speed);
            };

            vel = this.calcVel();

            draw = function () {
                this.vel = this.calcVel();
                this.v = this.v.add(new Vec3(0,0,0), this.v, this.vel);
                var x = this.v[0] / this.v[2];
                var y = this.v[1] / this.v[2];
                var x2 = this.v[0] / (this.v[2] + speed * 0.50);
                var y2 = this.v[1] / (this.v[2] + speed * 0.50);

                this.ctx.strokeStyle = this.color;
                this.ctx.beginPath();
                this.ctx.moveTo(x, y);
                this.ctx.lineTo(x2, y2);
                this.ctx.stroke();

                if (x < 0 - halfw || x > halfw || y < 0 - halfh || y > halfh) {
                    this.reset();
                }
            };

        }

        class Starfield {

            constructor(){
                this._init();
            }

            numOfStars = 1250;

            stars: any = [];

            _init() {
                for (var i = 0, len = this.numOfStars; i < len; i++) {
                    this.stars.push(new Star());
                }
            }

           

            draw = function () {
                this.ctx.translate(halfw, halfh);

                for (var i = 0, len = this.stars.length; i < len; i++) {
                    var currentStar = this.stars[i];

                    currentStar.draw();
                }
            };

        }

        var mStarField = new Starfield();

        function draw() {

            // make 5 seconds
            var millSeconds = 1000 * 10;

            var currentTime = new Date();

            speed = 0.025;

            this.ctx.setTransform(1, 0, 0, 1, 0, 0);
            this.ctx.fillStyle = 'rgba(0,0,0,0.2)';
            this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);

            mStarField.draw();

            window.requestAnimationFrame(draw);
        }

        draw();

        // window.onresize = function () {
        //     this.canvas.width = this.canvas.offsetWidth;
        //     this.canvas.height = this.canvas.offsetHeight;

        //     halfw = this.canvas.width / 6;
        //     halfh = this.canvas.height / 6;
        // };



    }
}
