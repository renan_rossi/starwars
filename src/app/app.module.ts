import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ServiceWorkerModule } from '@angular/service-worker';
import { TranslateModule } from '@ngx-translate/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { environment } from '@env/environment';
import { ToastrModule } from 'ngx-toastr';
import { SharedModule } from '@app/shared';
import { PlanetasModule } from './paginas/intro/planetas.module';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ApiService, Interceptor } from './services/api.service';
import { MatDialogModule } from '@angular/material/dialog';
import { Material } from './shared/material.module';
import { StartextComponent } from './components/startext/startext.component';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ServiceWorkerModule.register('./ngsw-worker.js', { enabled: environment.production }),
    FormsModule,
    HttpClientModule,
    TranslateModule.forRoot(),
    NgbModule,
    SharedModule,
    MatDialogModule,
    Material,
    ToastrModule.forRoot(), // ToastrModule added
    PlanetasModule,
    Interceptor,
    AppRoutingModule // must be imported as the last module as it contains the fallback route
  ],
  declarations: [AppComponent],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule {}
