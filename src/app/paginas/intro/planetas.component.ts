import { Component, OnInit } from '@angular/core';
import { fadeInFadeOut, slideRightAndFadeIn } from '@app/shared';


@Component({
  selector: 'app-planetas',
  templateUrl: './planetas.component.html',
  animations: [fadeInFadeOut, slideRightAndFadeIn]
})
export class PlanetasComponent implements OnInit {
  constructor() { }

  ngOnInit() { }

  dados = {
    apresentacao: {
      exibindoApresentacao: false,
      episodioApresentacao: "Episode I",
      tituloApresentacao: "A frontend hope",
      textoApresentacao: "It's a period of skill renewal.\n A young Padawan warrior hides at a secret base in Vitória - a small province of planet Earth on the Milky Way. \n \r The lazy forces mounted a blockade on the system to capture the young warrior and lead him to the dark side of the force. \n The young padawan seeks new places to hide from the lazy ones, and new masters to learn more about the hidden knowledge contained inside B2W. \n \r Meanwhile, commanded by senator Palpatinho, Darth Lazyer plans the massive attack to the home planet of B2W's masters, in a desperate hope to bargain with them the knowledge sought by the young padawan.",
      contagem: 0
    }
  };
  contagem: number = 0;
  interval: any;
  vm = {
    animacoesFiltro: [''],
    animacoesImagem: ['', 'bb-anim-exibir', 'bb-anim-inspecionar'],
    background: './assets/img/stars-bg-1.jpg',
    animacaoImagem: '',
    animacaoFiltro: ''
  };

  private variar = false;

  randomizarAnimacoes() {
    let indexFiltro = Math.floor(Math.random() * this.vm.animacoesFiltro.length);
    let indexImagem = Math.floor(Math.random() * this.vm.animacoesImagem.length);

    this.vm.animacaoFiltro = this.vm.animacoesFiltro[this.variar ? indexFiltro : 1];
    this.vm.animacaoImagem = this.vm.animacoesImagem[this.variar ? indexImagem : 1];
  }

  randomizarBackground(backgrounds: string[]) {
    const index = Math.floor(Math.random() * backgrounds.length);
    return backgrounds[this.variar ? index : 0];
  }

  substituirBackground(backgrounds: string[]) {
    this.vm.background = this.randomizarBackground(backgrounds);
    this.randomizarAnimacoes();
  }

  iniciarPararApresentacao() {
    if (!this.dados.apresentacao.exibindoApresentacao) {
      this.dados.apresentacao.exibindoApresentacao = true;
      window.open('https://open.spotify.com/track/7a9UUo3zfID7Ik2fTQjRLi?si=YG9Nj5l7QwCVUH0otT5D6Q', '_blank')   
      this.startTimer(90)

    } else {
      this.dados.apresentacao.exibindoApresentacao = false;
    };
  }


  startTimer(tempo: number) {
    this.pauseTimer();
    this.contagem = tempo;
    this.interval = setInterval(() => {
      if (this.contagem > 0) {
        
        this.contagem--;
      } else if (this.contagem == 0) {
        this.dados.apresentacao.exibindoApresentacao = false;
      }
    }, 1000)
  }

  pauseTimer() {
    clearTimeout(this.interval);
    this.interval = null;
  }




}




