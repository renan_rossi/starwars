import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { SharedModule } from '@app/shared';
import { PlanetasRoutingModule } from './planetas-routing.module';
import { PlanetasComponent } from './planetas.component';
import { Material } from '@app/shared/material.module';
import { FormsModule } from '@angular/forms';
import { StartextComponent } from '@app/components/startext/startext.component';
import { PlanetasAleatoriosComponent } from '@app/components/planetas/aleatorizar/planetas.aleatorios.component';
import { StarTravelComponent } from '@app/components/startravel/startravel.component';

@NgModule({
  imports: [CommonModule, TranslateModule, SharedModule, PlanetasRoutingModule, Material, FormsModule],
  declarations: [PlanetasComponent, PlanetasAleatoriosComponent, StartextComponent, StarTravelComponent],
  entryComponents: []
})
export class PlanetasModule {}
