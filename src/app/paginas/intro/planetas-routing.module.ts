import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PlanetasComponent } from './planetas.component';

const routes: Routes = [{ path: 'planetas', component: PlanetasComponent, data: {} }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class PlanetasRoutingModule {}
