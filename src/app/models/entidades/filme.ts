export class Filme{

    titulo: string
    id_episodio : string
    texto_abertura : string
    diretor : string
    produtor : string
    data_estreia : Date
    personagens:Array<string>
    planetas :Array<string> 
    naves:Array<string>
    veiculos :Array<string> 
    especies :Array<string>
    data_criacao: Date;
    data_edicao : Date;
    url : string
}