export class ListagemPaginada<T> {
  total: number;
  registros: Array<T>;
  anterior: string;
  proximo: string;

  constructor(){
    this.registros = new Array<T>();
  }
}
