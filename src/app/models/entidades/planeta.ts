import { Filme } from './filme';

export class Planeta {

  nome: string;
  periodo_rotacao: string;
  periodo_orbital: string;
  diametro: string;
  clima: string;
  gravidade: string;
  composicao_superficie: string;
  nivel_agua: string;
  populacao: string;
  residentes: Array<string>;
  _filmes: Array<string>;
  filmes: Array<Filme>
  data_criacao: Date;
  data_edicao: Date;
  url: string;



  referenciarFilmes(filmes: Array<Filme>) {
    this.filmes = filmes.filter(filme => filme.url === this._filmes.find(_filme => filme.url === _filme));
  }

}


