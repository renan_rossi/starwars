import { PlanetaDetalharResponse } from '../responses/planeta.detalhar.response';
import { Planeta } from '../entidades/planeta';

export class PlanetaMapper{

    static paraEntidade(orig : PlanetaDetalharResponse) : Planeta{
        
            var planeta = new Planeta();
        
            planeta.nome = orig.name;
            planeta.periodo_rotacao = orig.rotation_period;
            planeta.periodo_orbital = orig.orbital_period;
            planeta.diametro = orig.diameter;
            planeta.clima = orig.climate;
            planeta.gravidade = orig.gravity;
            planeta.composicao_superficie = orig.terrain;
            planeta.nivel_agua = orig.surface_water;
            planeta.populacao = orig.population;
            planeta.residentes = orig.residents;
            planeta._filmes = orig.films;
            planeta.data_criacao = new Date(orig.created);
            planeta.data_edicao = new Date(orig.edited);
            planeta.url = orig.url;
        
            return planeta;
    }

}