import { FilmeDetalharResponse } from '../responses/filme.detalhar.response';
import { Filme } from '../entidades/filme';


export class FilmeMapper{

    static paraEntidade(orig: FilmeDetalharResponse): Filme{
        var filme = new Filme();

        filme.titulo = orig.title;
        filme.id_episodio = orig.episode_id;
        filme.texto_abertura = orig.opening_crawl;
        filme.diretor = orig.director;
        filme.produtor = orig.producer;
        filme.data_estreia = new Date(orig.release_date);
        filme.personagens = orig.characters;
        filme.planetas = orig.planets;
        filme.naves = orig.starships;
        filme.veiculos = orig.vehicles;
        filme.especies = orig.species;
        filme.data_criacao = new Date(orig.created);
        filme.data_edicao = new Date(orig.edited);
        filme.url = orig.url;

        return filme;
    }


}