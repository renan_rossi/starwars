import { ListagemPaginadaResponse } from '../responses/lisagem.paginada.response';
import { ListagemPaginada } from '../entidades/listagem.paginada';

export class ListagemMapper {


    public static paraEntidade(orig: ListagemPaginadaResponse<any>): ListagemPaginada<any> {

        var listagem = new ListagemPaginada<any>()

        listagem.total = orig.count;
        listagem.anterior = orig.previous;
        listagem.proximo = orig.next;

        return listagem;
    }

}