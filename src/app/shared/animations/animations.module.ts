import { trigger, animate, transition, style } from '@angular/animations';

export const slideRightAndFadeIn = trigger('slideRightAndFadeIn', [
  transition(':enter', [
    style({
      transform: 'scale(1.2)',
      opacity: 0
    }),
    animate(
      '0.9s ease-out',
      style({
        transform: 'scale(1)',
        opacity: 1
      })
    )
  ]),
  transition(':leave', [
    // style({
    //     transform:  'scale(1)',
    //     opacity: 1
    // }),
    animate(
      '0.7s ease-in-out',
      style({
        transform: 'scale(1.2)',
        opacity: 0
      })
    )
  ])
]);

export const fadeInFadeOut = trigger('fadeInFadeOut', [
  transition(':enter', [style({ opacity: 0 }), animate('0.3s ease-out', style({ opacity: 1 }))]),
  transition(':leave', [style({ opacity: 1 }), animate('0.3s ease-in', style({ opacity: 0 }))])
]);
